(($) ->
  Backbone.PaginatedCollection = Backbone.Collection.extend(

    initialize: ->
      @currentPage = 1
      @perPage = 6
      @totalPages = 0

    parse: (resp)->
      @currentPage = resp.page_num
      @totalPages = resp.total_pages

    requestNextPage: (options) ->
      if @currentPage isnt `undefined`
        @currentPage += 1
        this.fetch()
      else
        response = new $.Deferred()
        response.reject()
        response.promise()

    info: ->
      info =
        currentPage: @currentPage
        totalPages:  @totalPages

      info
  )

) (window.jQuery || window.Zepto)