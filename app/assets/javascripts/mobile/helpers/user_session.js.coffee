# This is called when a user manually signs in.
DigMenu.update_current_user = (user_id)->
  DigMenu.currentUser.clear()
  DigMenu.currentUser.id = user_id
  DigMenu.currentUser.fetch(
    data: {auth_token: DigMenu.userSession.get_authentication_token()}
    processData: true
    success: (model, resp)->
      DigMenu.vent.trigger 'authentication:signed_in'

    error: (model, resp)->
  )      

DigMenu.addInitializer ->
  # Create useSession and current_user singleton if they don't exist.
  DigMenu.currentUser = new DigMenu.Models.User() unless DigMenu.currentUser
  DigMenu.userSession = new DigMenu.Models.UserSession() unless DigMenu.userSession