DigMenu.Helpers.parseQueryString = (queryString) ->
  params = {}
  if queryString
    _.each _.map(decodeURI(queryString).split(/&/g), (el, i) ->
      aux = el.split("=")
      o = {}
      if aux.length >= 1
        val = `undefined`
        val = aux[1]  if aux.length is 2
        o[aux[0]] = val
      o
    ), (o) ->
      _.extend params, o

  params