DigMenu.Helpers.FormHelpers = {};

DigMenu.Helpers.FormHelpers.fieldHelp = function(message) {
  return JST['templates/shared/form_field_help']({
    'message': message
  });
};
