DigMenu.Routers.AppRouter = Backbone.Marionette.AppRouter.extend(
  routes:
    '': "showRecipeIndex"
    'recipes': "showRecipeIndex"    
    'recipes?*queryString': "showRecipeIndex"    
    'recipes/:id': "showRecipe"
    'restaurants': "showRestaurantIndex"
    'restaurants/:id': "showRestaurant"
    'users/sign_in': "showSigninView"
    'users/:id': "showUserPage"

  initialize: (options)->

  showRecipe: (id) ->
    DigMenu.vent.trigger "recipes:show_recipe", id

  showRecipeIndex: (queryString)->
    query = DigMenu.Helpers.parseQueryString(queryString)
    DigMenu.vent.trigger "recipes:show_recipe_index", query 

  showRestaurant: (id) ->
    DigMenu.vent.trigger "restaurants:show_restaurant", id

  showRestaurantIndex: ->
    DigMenu.vent.trigger "restaurants:show_restaurant_index"

  showSigninView: ->
    DigMenu.vent.trigger "authentication:show_signin_view"

  showUserPage: (id)->
    DigMenu.vent.trigger "users:show_user_page", id
)

DigMenu.addInitializer ->
  DigMenu.appRouter = new DigMenu.Routers.AppRouter()
  DigMenu.appRouter.historyLength = 0
  DigMenu.appRouter.back = ->
    if DigMenu.appRouter.historyLength > 0
      window.history.go -2
      DigMenu.appRouter.historyLength -= 1
    else
      Backbone.history.navigate DigMenu.root, true

  DigMenu.appRouter.navigate = (fragment, options) ->
    DigMenu.appRouter.historyLength += 1
    Backbone.history.navigate fragment, options

  
  # All navigation that is relative should be passed through the navigate
  # method, to be processed by the router. If the link has a `data-bypass`
  # attribute, bypass the delegation completely.
  $(document).on "click", "a:not([data-bypass])", (evt) ->

    # Get the absolute anchor href.
    href =
      prop: $(this).prop("href")
      attr: $(this).attr("href")
    
    # Get the absolute root.
    root = location.protocol + "//" + location.host + DigMenu.root
    
    # Ensure the root is part of the anchor href, meaning it's relative.
    if href.prop and href.prop.slice(0, root.length) is root
      
      # Stop the default event to ensure the link will not cause a page
      # refresh.
      evt.preventDefault()
      
      # `Backbone.history.navigate` is sufficient for all Routers and will
      # trigger the correct events. The Router's internal `navigate` method
      # calls this anyways.  The fragment is sliced from the root.
      DigMenu.appRouter.navigate href.attr, true
