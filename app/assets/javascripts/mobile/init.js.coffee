window.DigMenu = new Backbone.Marionette.Application()
DigMenu.Views = {}
DigMenu.Views.Layouts = {}
DigMenu.Views.Regions = {}
DigMenu.Models = {}
DigMenu.Collections = {}
DigMenu.Routers = {}
DigMenu.Helpers = {}
DigMenu.root = "/"

# Instantiated global layouts
DigMenu.layouts = {}
DigMenu.addRegions topnav: "#topnav"
DigMenu.addRegions main: "#main"

Backbone.Marionette.Renderer.render = (template, data) ->
  throw "Template '" + template + "' not found!"  unless JST[template]
  JST[template] data

DigMenu.bind "initialize:after", ->
  DigMenu.main.show DigMenu.layouts.main
  DigMenu.topnav.show DigMenu.layouts.topnav

  enablePushState = true
  pushState = !!(enablePushState && window.history && window.history.pushState)
 
  Backbone.history.start(
      root: DigMenu.root
      silent:false, 
      pushState: pushState
  )

# This snipper should usually be loaded elsewhere
# It simply takes a <form> and converts its values to an object
$.fn.serializeObject = ->
  o = {}

  a = @serializeArray()
  $.each a, ->
    if o[@name] isnt `undefined`
      o[@name] = [o[@name]]  unless o[@name].push
      o[@name].push @value or ""
    else
      o[@name] = @value or ""
  o
