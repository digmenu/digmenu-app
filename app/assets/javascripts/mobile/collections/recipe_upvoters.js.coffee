DigMenu.Collections.RecipeUpvoters = Backbone.PaginatedCollection.extend(

  model: DigMenu.Models.User
  
  url: ->
    "/recipes/" + @restaurant_id + "/users_upvoted.json?" + $.param(
      page: @currentPage
      perPage: @perPage
    )

  initialize: (options)->
    @restaurant_id = options.restaurant_id
    @currentPage = 1
    @perPage = 3
    
  parse: (resp)->
    Backbone.PaginatedCollection.prototype.parse.call(@, resp)
    resp.voters

)