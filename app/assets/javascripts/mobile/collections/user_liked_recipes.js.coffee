DigMenu.Collections.UserLikedRecipe = Backbone.PaginatedCollection.extend(

  model: DigMenu.Models.Recipe
  
  url: ->
    "/users/" + @user_id + "/liked_recipes.json?" + $.param(
      page: @currentPage
      perPage: @perPage
    )

  initialize: (options)->
    @user_id = options.user_id
    @currentPage = 1    
    @perPage = 3

  parse: (resp)->
    Backbone.PaginatedCollection.prototype.parse.call(this, resp)
    resp.liked
)