DigMenu.Collections.RestaurantRecipe = Backbone.PaginatedCollection.extend(

  model: DigMenu.Models.Recipe
  
  url: ->
    "/restaurants/" + @restaurant_id + "/recipes.json?" + $.param(
      page: @currentPage
      perPage: @perPage
    )

  initialize: (options)->
    @restaurant_id = options.restaurant_id
    @currentPage = 1
    @perPage = 3
    
  parse: (resp)->
    Backbone.PaginatedCollection.prototype.parse.call(@, resp)
    resp.recipes

)