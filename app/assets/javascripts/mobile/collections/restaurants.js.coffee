DigMenu.Collections.Restaurant = Backbone.Collection.extend(

  model: DigMenu.Models.Restaurant

  url: '/restaurants'
)