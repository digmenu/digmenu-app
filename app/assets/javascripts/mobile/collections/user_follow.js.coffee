DigMenu.Collections.UserFollow = Backbone.PaginatedCollection.extend(

  model: DigMenu.Models.User
  
  url: ->
    "/users/" + @user_id + "/" + @action + ".json?" + $.param(
      page: @currentPage
      perPage: @perPage
    )

  initialize: (options)->
    @user_id = options.user_id
    @action = options.action
    @currentPage = 1    
    @perPage = 6

  parse: (resp)->
    Backbone.PaginatedCollection.prototype.parse.call(this, resp)
    resp.follows
)