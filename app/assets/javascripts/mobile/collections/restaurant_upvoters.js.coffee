DigMenu.Collections.RestaurantUpvoters = Backbone.PaginatedCollection.extend(

  model: DigMenu.Models.User
  
  url: ->
    "/restaurants/" + @restaurant_id + "/users_upvoted.json?" + $.param(
      page: @currentPage
      perPage: @perPage
    )

  initialize: (options)->
    @restaurant_id = options.restaurant_id
    @currentPage = 1
    @perPage = 3
    
  parse: (resp)->
    Backbone.PaginatedCollection.prototype.parse.call(@, resp)
    resp.voters

)