DigMenu.Collections.Recipe = Backbone.PaginatedCollection.extend(

  model: DigMenu.Models.Recipe
  
  url: ->
    "/recipes.json?" + $.param(
      page: @currentPage
      perPage: @perPage
      q: @query
    )

  initialize: (options)->
    @currentPage = 1
    @perPage = 6
    @query = ""
    @query = options.query if options and options.query
      

  parse: (resp)->
    Backbone.PaginatedCollection.prototype.parse.call(@, resp)
    resp.recipes

)