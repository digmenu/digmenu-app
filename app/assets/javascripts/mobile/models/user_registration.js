DigMenu.Models.UserRegistration = Backbone.Model.extend({
  urlRoot: '/api/users/sign_up.json',

  paramRoot: 'user',

  defaults: {
    "email": "",
    "password": "",
    "password_confirmation": ""
  }
});
