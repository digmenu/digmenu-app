DigMenu.Models.Restaurant = Backbone.Model.extend(
  urlRoot : '/restaurants'

  initialize: ->

  parse: (resp, options)->
    resp

  vote_up: (options)->
    $.ajax
      url: "/restaurants/" + @id + "/vote_up"
      type: "POST"
      dataType: "json"
      success: options.success    
)
