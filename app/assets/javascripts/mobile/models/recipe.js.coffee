DigMenu.Models.Recipe = Backbone.Model.extend(
  urlRoot : '/recipes'

  initialize: ->

  parse: (resp, options)->
    resp

  vote_up: (options)->
    $.ajax
      url: "/recipes/" + @id + "/vote_up"
      type: "POST"
      dataType: "json"
      success: options.success

)
