DigMenu.Models.UserSession = Backbone.Model.extend(
  urlRoot: ->
    @action

  paramRoot: 'user',

  defaults:
    "remember_me": true

  initialize: ->
    @action = ""

  parse: (resp, options)->
    resp

  authenticated: ->
    (@get('authentication_token') isnt undefined)

  get_authentication_token: ->
    if @get('authentication_token')
      @get('authentication_token')

  success_sign_in: (model, resp)->
    DigMenu.update_current_user(model.get('user_id'))

  error_sign_in: (model, resp)->
    DigMenu.currentUser.clear() if DigMenu.currentUser
    DigMenu.userSession.clear() if DigMenu.userSession

  success_sign_up: (model, resp)->
    DigMenu.update_current_user(resp)
    DigMenu.vent.trigger 'authentication:signed_up'

  error_sign_up: (model, resp)->
    DigMenu.currentUser.clear() if DigMenu.currentUser
    DigMenu.userSession.clear() if DigMenu.userSession

  success_sign_out: ()->      
    DigMenu.currentUser.clear()
    DigMenu.userSession.clear()
    DigMenu.vent.trigger 'authentication:signed_out'

  sign_in: (creds)->
    @action = "/api/users/sign_in.json"
    @save(creds,
      success: @success_sign_in
      error: @error_sign_in
    )

  sign_out: ->
    auth_token = @get_authentication_token()
    $.ajax({
      url: "/api/users/sign_out?auth_token="+auth_token, 
      type: 'delete',
      dataType: "json",
      beforeSend: (xhr) ->
        xhr.setRequestHeader "auth_token", auth_token
    })

    @success_sign_out()
    
  sign_up: (creds)->
    @action = "/api/users/signup.json"
    @save(creds,
      success: @success_sign_up
      error: @error_sign_up
    )
)
