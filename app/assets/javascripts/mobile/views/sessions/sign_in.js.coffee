DigMenu.Views.UserSignin = Backbone.Marionette.ItemView.extend(

  template: 'mobile/templates/sessions/sign_in'

  events:
    "submit form.signin": "sign_in"
    'click a.close-x' : 'cancel_sign_in'

  initialize: ->
    @model = @options.model

  sign_in: (ev)->
    # Disable the button
    $('[type=submit]', ev.currentTarget).val('Signing in..').attr('disabled', 'disabled');
    # Serialize the form into an object using a jQuery plgin
    creds = $(ev.currentTarget).serializeObject();
    console.log creds
    @model.sign_in(creds);
    false

  cancel_sign_in: (e)->
    e.preventDefault()
    DigMenu.vent.trigger 'authentication:cancel_sign_in'      
)