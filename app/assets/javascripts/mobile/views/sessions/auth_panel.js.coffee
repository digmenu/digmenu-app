DigMenu.Views.AuthPanel = Backbone.Marionette.ItemView.extend(

  id: "authentication-panel"

  template: 'mobile/templates/sessions/auth_panel'

  events: 
    'click a#signout': 'sign_out'
    'click a#signin': 'sign_in'

  initialize: ()->
    @model = @options.model

  sign_in: (e)->
    e.preventDefault()
    DigMenu.vent.trigger 'authentication:show_signin_view'    

  sign_out: (e)->
    e.preventDefault()
    @model.sign_out()
    DigMenu.vent.trigger 'authentication:sign_out'

  serializeData: ->
    authenticated: DigMenu.userSession.authenticated()
    username: DigMenu.currentUser.get('name')
    userid: DigMenu.currentUser.get('id')

)