DigMenu.Views.Layouts.Main = Backbone.Marionette.Layout.extend(
  className: "main"
  template: "mobile/templates/layouts/main"
  regions:
    lightbox: 
      selector: "#lightbox"
      regionType: DigMenu.Views.Regions.Lightbox
    content: 
      selector: "#content"
      regionType: DigMenu.Views.Regions.Content

  views: {}
  initialize: ->
    @views.showRecipe = DigMenu.Views.RecipeShow
    @views.RecipesIndex = DigMenu.Views.RecipesIndex
    @views.showRestaurant = DigMenu.Views.RestaurantShow
    @views.RestaurantIndex = DigMenu.Views.RestaurantIndex
    @views.UserSignin = DigMenu.Views.UserSignin
    @views.UserShow = DigMenu.Views.UserShow

    DigMenu.vent.on "recipes:show_recipe", @showRecipe, this
    DigMenu.vent.on "recipes:show_recipe_index", @showRecipeIndex, this
    DigMenu.vent.on "restaurants:show_restaurant", @showRestaurant, this
    DigMenu.vent.on "restaurants:show_restaurant_index", @showRestaurantIndex, this
    DigMenu.vent.on "authentication:show_signin_view", @showSigninView, this
    DigMenu.vent.on "authentication:cancel_sign_in", @cancel_sign_in, this
    DigMenu.vent.on "authentication:signed_in", @onUserSignedIn, this
    DigMenu.vent.on "navigation:back", @onNavigateBack, this
    DigMenu.vent.on "authentication:show_dropdown_nav_menu", @onShowDropdownNavMenu, this
    DigMenu.vent.on "authentication:collapse_dropdown_nav_menu", @onCloseDropdownNavMenu, this
    DigMenu.vent.on "users:show_user_page", @onShowUserPage, this
    DigMenu.vent.on "search:cancel", @onCancelSearch, this

  showSigninView: ->
    @lightbox.show new @views.UserSignin(model: DigMenu.userSession)
  
  onUserSignedIn: ->
    #DigMenu.appRouter.navigate DigMenu.root, true
    @lightbox.close()

  cancel_sign_in: ->
    @lightbox.close()

  showRecipe: (id) ->
    model = new DigMenu.Models.Recipe(id: id)
    model.fetch success: $.proxy(@success_fetch_receipe, @)

  success_fetch_receipe: (model, resp, options) ->
    @content.show new @views.showRecipe(model: model)

  showRecipeIndex: (options)->
    @showSpinner()

    collection = new DigMenu.Collections.Recipe(options)
    collection.fetch
      success: $.proxy(@success_fetch_recipe_collection, @)
      error: $.proxy(@error_fetch_recipe_collection, @)

  success_fetch_recipe_collection: (model, resp, options) ->
    @hideSpinner()
    @content.show new @views.RecipesIndex(collection: model)

  error_fetch_recipe_collection: (model, resp, options) ->

  showRestaurant: (id) ->
    model = new DigMenu.Models.Restaurant(id: id)
    model.fetch success: $.proxy(@success_fetch_restaurant, @)

  success_fetch_restaurant: (model, resp, options) ->
    @content.show new @views.showRestaurant(model: model)

  showRestaurantIndex: ->
    @showSpinner()

    collection = new DigMenu.Collections.Restaurant
    collection.fetch
      success: $.proxy(@success_fetch_restaurant_collection, @)
      error: $.proxy(@error_fetch_restaurant_collection, @)

  success_fetch_restaurant_collection: (model, resp, options) ->
    @hideSpinner()
    @content.show new @views.RestaurantIndex(collection: model)

  error_fetch_restaurant_collection: (model, resp, options) ->

  onShowUserPage: (id)->
    model = new DigMenu.Models.User(id: id)
    model.fetch success: $.proxy(@success_fetch_user, @)

  success_fetch_user: (model, resp, options)->
    @content.show new @views.UserShow(model: model)

  onNavigateBack: ->
    DigMenu.appRouter.back()

  onShowDropdownNavMenu: ->
    @lightbox.show new DigMenu.Views.Search

  onCloseDropdownNavMenu: ->
    @lightbox.close()

  onCancelSearch: ->
    @lightbox.close()

  showSpinner: ->
    $("#loading").show()

  hideSpinner: ->
    $("#loading").hide()
)

DigMenu.addInitializer ->
  DigMenu.layouts.main = new DigMenu.Views.Layouts.Main()
