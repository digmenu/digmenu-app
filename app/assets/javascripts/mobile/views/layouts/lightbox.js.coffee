DigMenu.Views.Regions.Lightbox = Backbone.Marionette.Region.extend(
  el: '#lightbox'

  initialize: ->

  onShow: ->
    this.$el.show()
    $("body").css "overflow", "hidden"

  onClose: ->
    $("#lightbox").hide()
    $("body").css "overflow", ""
    $("#lightbox").off()
    $(document).off ".lightbox"
)