DigMenu.Views.Layouts.TopNav = Backbone.Marionette.Layout.extend(
  id: "topnavbar"
  template: "mobile/templates/layouts/topnav"
  regions:
    auth_panel: "#auth-panel-container"

  views: {}
  events:
    "click a#nav_back": "navigate_back"
    "click div#nav-dropdown-button": "show_dropdown_nav_menu"

  initialize: ->
    @views.auth_panel = DigMenu.Views.AuthPanel

    @dropdownMenuCollapsed = true

    DigMenu.vent.on "authentication:signed_in", @onShow, this
    DigMenu.vent.on "authentication:signed_out", @onShow, this

  navigate_back: (e) ->
    e.preventDefault()
    DigMenu.vent.trigger "navigation:back"

  show_dropdown_nav_menu: ->
    if @dropdownMenuCollapsed is true
      DigMenu.vent.trigger "authentication:show_dropdown_nav_menu"
    else
      DigMenu.vent.trigger "authentication:collapse_dropdown_nav_menu"

    @dropdownMenuCollapsed = !@dropdownMenuCollapsed

  onShow: ->
    @auth_panel.show new @views.auth_panel(model: DigMenu.userSession)
)

DigMenu.addInitializer ->
  DigMenu.layouts.topnav = new DigMenu.Views.Layouts.TopNav()
