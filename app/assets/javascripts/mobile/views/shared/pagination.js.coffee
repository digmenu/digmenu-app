DigMenu.Views.Pagination = Backbone.Marionette.ItemView.extend(
  tagName: "aside"
  template: 'mobile/templates/shared/pagination'

  events:
    'click a.servernext': 'nextResultPage'

  initialize: ->
    @collection.on('reset', @render, @)
    @collection.on('change', @render, @)

  nextResultPage: (e)->
    e.preventDefault()
    @collection.requestNextPage()

  serializeData: ->
    @collection.info()

)