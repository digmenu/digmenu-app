DigMenu.Views.Search = Backbone.Marionette.ItemView.extend(
  id: "search"
  template: 'mobile/templates/search/search'  

  events: 
    'submit #queryForm' : 'doSearch'
    'click a.close-x' : 'onCancelSearch'

  initialize: ->
    @model = @options.model

  doSearch: ->
    query = $('#searchbox').val()
    if (query)
      searchBy = $('#searchBy').val()

    DigMenu.appRouter.navigate "recipes?query="+query, true

    @close()

  onCancelSearch: ->
    DigMenu.vent.trigger 'search:cancel'
)