DigMenu.Views.DropdownNavMenu = Backbone.Marionette.ItemView.extend(
  id: "dropdown-nav-menu"

  template: 'mobile/templates/navigations/dropdown_nav_menu'

  initialize: ()->

  serializeData: ->
)