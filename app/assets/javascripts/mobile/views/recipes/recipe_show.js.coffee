DigMenu.Views.RecipeShowRecipeCard = Backbone.Marionette.ItemView.extend(
  className: "recipe-card"
  template: 'mobile/templates/recipes/recipe_card'  

  events: 
    'click button#recipe_vote_btn' : 'onVoteupRecipe'

  initialize: ->
    @model = @options.model
    @listenTo(@model, "change", @modelChange)

  onRender: ->
    @modelChange()

  modelChange: (model, val)->
    if @model.get('is_liked_by_me')
      heart = this.$el.find('.icon-heart')
      heart.removeClass('icon-heart')
      heart.addClass('icon-heart-red')        
      this.$el.find('.like_count').html(@model.get('like_count'))

  onVoteupRecipe: ->
    if DigMenu.userSession.authenticated()
      @model.vote_up(success: $.proxy(@success_vote_up, @)) if @model
    else
      DigMenu.vent.trigger "authentication:show_signin_view"

  success_vote_up: (model, resp) ->
    if resp is "success" and model.success is true
      @model.set('is_liked_by_me', true);       
      @model.set('like_count', @model.get('like_count') + 1);

)

DigMenu.Views.RecipeUpVoter = Backbone.Marionette.ItemView.extend(
  className: "follow-item"
  template: 'mobile/templates/users/item_follow'
 
  initialize: ->
    @model = @options.model

  remove: ->
)

DigMenu.Views.RecipeShowUpVoters = Backbone.Marionette.CompositeView.extend(
  className: "users-upvoted"
  itemView: DigMenu.Views.RecipeUpVoter
  template: 'mobile/templates/users/show_follow'

  initialize: ->
    @model = @options.model

    @collection = new DigMenu.Collections.RecipeUpvoters(restaurant_id: @model.get('id'))
    @paginationView = new DigMenu.Views.Pagination(collection: @collection)

    @collection.fetch()

  appendHtml: (collectionView, itemView, index)->
    collectionView.$('.follow-list').append(itemView.el) if @collection.totalPages

  onRender: ->
    @paginationView.render()
    this.$('#pagination').append(@paginationView.el)
)

DigMenu.Views.RecipeShow = Backbone.Marionette.Layout.extend(
  className: "recipe-show"
  template: 'mobile/templates/recipes/show'

  regions: {
    recipeCard: '#recipe-card'
    tabContent: '#tab-content'
  },

  views: {}

  events:
    'click ul li a': 'switchViews'

  initialize: ->
    @model = @options.model

    @views.recipeCard = DigMenu.Views.RecipeShowRecipeCard
    @views.upVoters = DigMenu.Views.RecipeShowUpVoters

  onShow: ->
    @recipeCard.show new @views.recipeCard(model: @options.model)
    @tabContent.show new @views.upVoters(model: @options.model)

  switchViews: (ev)->
    ev.preventDefault()
    switch $(ev.currentTarget).data('content')
      when "upVoters"
        @tabContent.show new @views.upVoters(model: @options.model)    
)