DigMenu.Views.RecipeItem = Backbone.Marionette.ItemView.extend(
  className: "recipe-card"
  template: 'mobile/templates/recipes/item'

  events: 
    'click a.like' : 'onVoteupRecipe'
 
  initialize: ->
    @model = @options.model
    this.listenTo(@model, "change", @modelChange)

  remove: ->

  onRender: ->
    @modelChange()

  modelChange: (model, val)->
    if @model.get('is_liked_by_me')
      heart = this.$el.find('.icon-heart')
      heart.removeClass('icon-heart')
      heart.addClass('icon-heart-red')    
      this.$el.find('.like_count').html(@model.get('like_count'))
    
  onVoteupRecipe: ->
    if DigMenu.userSession.authenticated()
      @model.vote_up(success: $.proxy(@success_vote_up, @)) if @model
    else
      DigMenu.vent.trigger "authentication:show_signin_view"

  success_vote_up: (model, resp) ->
    if resp is "success" and model.success is true
      @model.set('is_liked_by_me', true);      
      @model.set('like_count', @model.get('like_count') + 1);

)

DigMenu.Views.RecipesIndex = Backbone.Marionette.CompositeView.extend(
  className: "recipe-groups-show"
  template: 'mobile/templates/recipes/index'

  itemView: DigMenu.Views.RecipeItem

  views: []

  collectionEvents:
    "remove": "modelRemoved"

  initialize: ->
    @collection = @options.collection
    
    @paginationView = new DigMenu.Views.Pagination(collection: @collection)

  appendHtml: (collectionView, itemView, index)->
    collectionView.$('.recipe-groups').append(itemView.el)
    
  onCompositeRendered: ->
    @paginationView.render()
    this.$('#pagination').append(@paginationView.el)

  modelRemoved: ->
)