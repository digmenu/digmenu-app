DigMenu.Views.UserShowUserCard = Backbone.Marionette.ItemView.extend(
  className: "user-card"
  template: 'mobile/templates/users/user_card'  
  events:
    "click button#user_follow_btn": "user_follow"

  initialize: ->
    @model = @options.model

    @initUserFollowship()

  initUserFollowship: ->
    followship = 
      followed_id: @model.get('id')

    if @model.get('id') isnt DigMenu.currentUser.get('id')
      @userFollowship = new DigMenu.Models.UserFollowShip(
        id: @model.get('followship').id
        followship: followship
        auth_token: DigMenu.userSession.get_authentication_token())

      @userFollowship.on("change", @onChangeUserFollowship, this)  

  onRender: ->
    if @userFollowship
      if @userFollowship.get('id') isnt undefined
        this.$('#user_follow_btn').html("unfollow")
      else
        this.$('#user_follow_btn').html("follow")
    else
      this.$('#user_follow_btn').hide()

  onChangeUserFollowship: ->
    if @userFollowship
      if @userFollowship.get('id') isnt undefined
        this.$('#user_follow_btn').html("unfollow")
        @model.set('follower_count', @model.get('follower_count') + 1)
      else
        this.$('#user_follow_btn').html("follow")
        @model.set('follower_count', @model.get('follower_count') - 1)

      this.$('#follower_count').html(@model.get('follower_count'))

  user_follow: ->
    if @userFollowship.get('id') isnt undefined
      @userFollowship.destroy(
        success: $.proxy(@clearUserFollowship, @)
      )
    else
      @userFollowship.save()

  clearUserFollowship: ->
    @userFollowship.unset('id')      
)

DigMenu.Views.UserFollowItem = Backbone.Marionette.ItemView.extend(
  className: "follow-item"
  template: 'mobile/templates/users/item_follow'
 
  initialize: ->
    @model = @options.model

  remove: ->
)

DigMenu.Views.UserShowFollowers = Backbone.Marionette.CompositeView.extend(
  className: "user-followers"
  itemView: DigMenu.Views.UserFollowItem
  template: 'mobile/templates/users/show_follow'  

  initialize: ->
    user_id = @options.model.get('id')
    @collection = new DigMenu.Collections.UserFollow(user_id: user_id, action: "followers")
    @paginationView = new DigMenu.Views.Pagination(collection: @collection)

    @collection.fetch()

  appendHtml: (collectionView, itemView, index)->
    collectionView.$('.follow-list').append(itemView.el) if @collection.totalPages

  onRender: ->
    @paginationView.render()
    this.$('#pagination').append(@paginationView.el)
)

DigMenu.Views.UserShowFollowings = Backbone.Marionette.CompositeView.extend(
  className: "user-followings"
  itemView: DigMenu.Views.UserFollowItem
  template: 'mobile/templates/users/show_follow'

  initialize: ->
    user_id = @options.model.get('id')
    @collection = new DigMenu.Collections.UserFollow(user_id: user_id, action: "following")
    @paginationView = new DigMenu.Views.Pagination(collection: @collection)

    @collection.fetch()

  appendHtml: (collectionView, itemView, index)->
    collectionView.$('.follow-list').append(itemView.el) if @collection.totalPages

  onRender: ->
    @paginationView.render()
    this.$('#pagination').append(@paginationView.el)
)

DigMenu.Views.UserRecipeItem = Backbone.Marionette.ItemView.extend(
  className: "recipe-card"
  template: 'mobile/templates/users/item_recipe'
 
  initialize: ->
    @model = @options.model

  remove: ->
)

DigMenu.Views.UserShowLikedRecipes = Backbone.Marionette.CompositeView.extend(
  className: "user-liked-recipes"
  itemView: DigMenu.Views.UserRecipeItem
  template: 'mobile/templates/users/liked_recipes'  

  initialize: ->
    user_id = @options.model.get('id')
    @collection = new DigMenu.Collections.UserLikedRecipe(user_id: user_id)
    @paginationView = new DigMenu.Views.Pagination(collection: @collection)

    @collection.fetch()

  appendHtml: (collectionView, itemView, index)->
    collectionView.$('.recipe-groups').append(itemView.el) if @collection.totalPages

  onRender: ->
    @paginationView.render()
    this.$('#pagination').append(@paginationView.el)

)

DigMenu.Views.UserShow = Backbone.Marionette.Layout.extend(
  className: "user-show"
  template: 'mobile/templates/users/user_show'
  regions: {
    userCard: '#user-card'
    tabContent: '#tab-content'
  },

  views: {}

  events:
    'click ul li a': 'switchViews'

  initialize: ->
    @views.userCard = DigMenu.Views.UserShowUserCard
    @views.likedRecipes = DigMenu.Views.UserShowLikedRecipes
    @views.followers = DigMenu.Views.UserShowFollowers
    @views.following = DigMenu.Views.UserShowFollowings

  onShow: ->
    @userCard.show new @views.userCard(model: @options.model)
    @tabContent.show new @views.likedRecipes(model: @options.model)

  switchViews: (ev)->
    ev.preventDefault()
    switch $(ev.currentTarget).data('content')
      when "likedRecipes"
        @tabContent.show new @views.likedRecipes(model: @options.model)
      when "followers"
        @tabContent.show new @views.followers(model: @options.model)
       when "following"
        @tabContent.show new @views.following(model: @options.model)
)