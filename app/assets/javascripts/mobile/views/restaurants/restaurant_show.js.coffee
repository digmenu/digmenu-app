DigMenu.Views.RestaurantShowRestaurantCard = Backbone.Marionette.ItemView.extend(
  className: "restaurant-card"
  template: 'mobile/templates/restaurants/restaurant_card'  

  events: 
    'click button#restaurant_vote_btn' : 'onVoteupRestaurant'

  initialize: ->
    @model = @options.model
    this.listenTo(@model, "change", @modelChange)

  onRender: ->
    @modelChange()

  modelChange: (model, val)->
    if @model.get('is_liked_by_me')
      heart = this.$el.find('.icon-heart')
      heart.removeClass('icon-heart')
      heart.addClass('icon-heart-red')    
      this.$el.find('.like_count').html(@model.get('like_count'))

  onVoteupRestaurant: ->
    if DigMenu.userSession.authenticated()
      @model.vote_up(success: $.proxy(@success_vote_up, @)) if @model
    else
      DigMenu.vent.trigger "authentication:show_signin_view"

  success_vote_up: (model, resp) ->
    if resp is "success" and model.success is true
      @model.set('is_liked_by_me', true);      
      @model.set('like_count', @model.get('like_count') + 1);
)

DigMenu.Views.RestaurantRecipeItem = Backbone.Marionette.ItemView.extend(
  className: "recipe-card"
  template: 'mobile/templates/users/item_recipe'
 
  initialize: ->
    @model = @options.model

  remove: ->
)

DigMenu.Views.RestaurantShowStarred = Backbone.Marionette.CompositeView.extend(
  className: "starred-recipes"
  itemView: DigMenu.Views.RestaurantRecipeItem
  template: 'mobile/templates/restaurants/recipe_group'  

  initialize: ->
    @model = @options.model

    @collection = new DigMenu.Collections.RestaurantRecipe(restaurant_id: @model.get('id'))
    @paginationView = new DigMenu.Views.Pagination(collection: @collection)
    
    @collection.fetch()
    
  appendHtml: (collectionView, itemView, index)->
    collectionView.$(".recipe-groups").append(itemView.el) if @collection.totalPages

  onRender: ->
    @paginationView.render()
    this.$('#pagination').append(@paginationView.el)    

)

DigMenu.Views.RestaurantRecipeItem = Backbone.Marionette.ItemView.extend(
  className: "recipe-card"
  template: 'mobile/templates/users/item_recipe'
 
  initialize: ->
    @model = @options.model

  remove: ->
)

DigMenu.Views.RestaurantShowRecipes = Backbone.Marionette.CompositeView.extend(
  className: "all-recipes"
  itemView: DigMenu.Views.RestaurantRecipeItem
  template: 'mobile/templates/restaurants/recipe_group'

  initialize: ->
    @model = @options.model

    @collection = new DigMenu.Collections.RestaurantRecipe(restaurant_id: @model.get('id'))
    @paginationView = new DigMenu.Views.Pagination(collection: @collection)
    
    @collection.fetch()
    
  appendHtml: (collectionView, itemView, index)->
    collectionView.$(".recipe-groups").append(itemView.el) if @collection.totalPages

  onRender: ->
    @paginationView.render()
    this.$('#pagination').append(@paginationView.el)    
)

DigMenu.Views.RestaurantUpVoter = Backbone.Marionette.ItemView.extend(
  className: "follow-item"
  template: 'mobile/templates/users/item_follow'
 
  initialize: ->
    @model = @options.model

  remove: ->
)

DigMenu.Views.RestaurantShowUpVoters = Backbone.Marionette.CompositeView.extend(
  className: "users-upvoted"
  itemView: DigMenu.Views.RestaurantUpVoter
  template: 'mobile/templates/users/show_follow'

  initialize: ->
    @model = @options.model

    @collection = new DigMenu.Collections.RestaurantUpvoters(restaurant_id: @model.get('id'))
    @paginationView = new DigMenu.Views.Pagination(collection: @collection)

    @collection.fetch()

  appendHtml: (collectionView, itemView, index)->
    collectionView.$('.follow-list').append(itemView.el) if @collection.totalPages

  onRender: ->
    @paginationView.render()
    this.$('#pagination').append(@paginationView.el)
)

DigMenu.Views.RestaurantShow = Backbone.Marionette.Layout.extend(
  className: "restaurant-show"
  template: 'mobile/templates/restaurants/show'

  regions: {
    restaurantCard: '#restaurant-card'
    tabContent: '#tab-content'
  },

  views: {}

  events:
    'click ul li a': 'switchViews'

  initialize: ->
    @views.restaurantCard = DigMenu.Views.RestaurantShowRestaurantCard
    @views.starredRecipes = DigMenu.Views.RestaurantShowStarred
    @views.recipes = DigMenu.Views.RestaurantShowRecipes
    @views.upVoters = DigMenu.Views.RestaurantShowUpVoters

  onShow: ->
    @restaurantCard.show new @views.restaurantCard(model: @options.model)
    @tabContent.show new @views.recipes(model: @options.model)

  switchViews: (ev)->
    ev.preventDefault()
    switch $(ev.currentTarget).data('content')
      when "recipes"
        @tabContent.show new @views.recipes(model: @options.model)
      when "starredRecipes"
        @tabContent.show new @views.starredRecipes(model: @options.model)
      when "upVoters"
        @tabContent.show new @views.upVoters(model: @options.model)        
)