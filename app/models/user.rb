class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable, 
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :token_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  has_many :authentications

  # Setup accessible (or protected) attributes for your model
  attr_accessible :role_ids, :as => :admin
  attr_accessible :name, :email, :password, :authentication_token,
    :password_confirmation, :remember_me, :provider, :uid
  attr_accessible :avatar

  acts_as_voter
  acts_as_tagger

  has_many :followships, :foreign_key => "follower_id",
                         :dependent   => :destroy
  has_many :reverse_followships, :dependent => :destroy,
                                   :foreign_key => "followed_id",
                                   :class_name => "Followship"                         
  has_many :following, :through => :followships, 
                       :source => :followed
  has_many :followers, :through => :reverse_followships,
					             :source => :follower

  has_many :restaurants                     
  
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" },
    :default_url => "/assets/avatars/:style/default_user.png"

  #validates_attachment_presence :avatar

  paginates_per 10

  before_save :ensure_authentication_token

  def following?(followed)
    followships.find_by_followed_id(followed)
  end

  def self.from_omniauth(auth)
    where(auth.slice(:provider, :uid)).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.name = auth.info.name
      user.oauth_token = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.save!
    end
  end

  def self.find_for_facebook_oauth(access_token, signed_in_resource=nil)
    data = access_token.extra.raw_info
    if data == "false"
      self.new
    elsif user = self.find_by_email(data.email)
      user
    else # Create a user with a stub password. 
      self.create!(:email => data.email, :password => Devise.friendly_token[0,20]) 
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  def follow!(followed)
    followships.create!(:followed_id => followed.id)
  end

  def unfollow!(followed)
    followships.find_by_followed_id(followed).destroy
  end

  def avatar_url
    avatar.url(:thumb)
  end

  def avatar_medium_url
    avatar.url(:medium)
  end  

  #def as_json(options = nil)
  #  hash = serializable_hash(options)
  #end

  def serializable_hash(options={})
    options = {
      :only => [:id, :name, :authentication_token]
    }.update(options)
    super(options)
  end
  
  #def Self.liked_restaurants(user)
  #  return nil
  #end

  def self.liked_recipes(user)
    voted_recipes = User.find_by_sql("Select * from votes where voter_id = #{user.id}")
    return voted_recipes
  end

end
