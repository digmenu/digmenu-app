class Recipe < ActiveRecord::Base
  attr_accessible :name, :restaurant_id, :description
  attr_accessible :recipe_images_attributes
  attr_accessible :category_list, :alert_list, :taste_list

  belongs_to :restaurant

  acts_as_votable
  acts_as_taggable
  acts_as_taggable_on :tags, :category, :alert, :taste

  validates :name, :presence => true

  has_many :recipe_images, :dependent => :destroy

  has_many :taggings, :as => :taggable, :dependent => :destroy, :include => :tag, 
          :class_name => "ActsAsTaggableOn::Tagging",
          :conditions => "taggings.taggable_type = 'Recipe'"
          
  #for context-dependent tags:
  has_many :category_tags, :through => :taggings, :source => :tag, :class_name => "ActsAsTaggableOn::Tag",
          :conditions => "taggings.context = 'category'"
  has_many :alert_tags, :through => :taggings, :source => :tag, :class_name => "ActsAsTaggableOn::Tag",
          :conditions => "taggings.context = 'alert'"
  has_many :taste_tags, :through => :taggings, :source => :tag, :class_name => "ActsAsTaggableOn::Tag",
          :conditions => "taggings.context = 'taste'"

  # default per_page                   
  paginates_per 10

  accepts_nested_attributes_for :recipe_images, :reject_if => proc { |attributes| attributes['photo'].blank? }, :allow_destroy => true
  #accepts_nested_attributes_for :category_list, :reject_if => proc { |attributes| attributes.blank? }, :allow_destroy => true
  #accepts_nested_attributes_for :alert_list, :reject_if => proc { |attributes| attributes.blank? }, :allow_destroy => true
  #accepts_nested_attributes_for :taste_list, :reject_if => proc { |attributes| attributes.blank? }, :allow_destroy => true

  #, :reject_if => lambda { |r| r['recipe_image'].nil? }
  
  def avatar_url
    return recipe_images[0].photo_small_url if recipe_images
  end
end