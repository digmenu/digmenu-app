class Restaurant < ActiveRecord::Base
  attr_accessible :category, :address, :city, :country, :region, :description, :name
  attr_accessible :restaurant_images_attributes
  attr_accessible :avatar

  acts_as_votable
  acts_as_gmappable
  
  belongs_to :user
  has_many :recipes, :dependent => :destroy
  #has_many :restaurant_images, :dependent => :destroy
  #accepts_nested_attributes_for :restaurant_images, :reject_if => proc { |attributes| attributes['photo'].blank? }, :allow_destroy => true
  #accepts_nested_attributes_for :category_list, :reject_if => proc { |attributes| attributes.blank? }, :allow_destroy => true
  #accepts_nested_attributes_for :alert_list, :reject_if => proc { |attributes| attributes.blank? }, :allow_destroy => true
  #accepts_nested_attributes_for :taste_list, :reject_if => proc { |attributes| attributes.blank? }, :allow_destroy => true
  #, :reject_if => lambda { |r| r['restaurant_image'].nil? }

  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" },
    :default_url => "/assets/avatars/:style/default_restaurant.png"

  geocoded_by :location
  after_validation :geocode # auto-fetch coordinates
  
  #validates_attachment_presence :avatar

  paginates_per 10

  def avatar_url
    avatar.url(:thumb)
  end

  def avatar_medium_url
    avatar.url(:medium)
  end
  
  #def serializable_hash(options = {})
  #  super(:include => [:restaurant, :recipe_images])
  #end

  def gmaps4rails_address
  #describe how to retrieve the address from your model, if you use directly a db column, you can dry your code, see wiki
    "#{self.address}, #{self.city}, #{self.country}" 
  end
end
