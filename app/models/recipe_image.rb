class RecipeImage < ActiveRecord::Base
  attr_accessible :caption, :recipe_id, :photo

  belongs_to :recipe

  #accepts_nested_attributes_for :recipe
  
  has_attached_file :photo, :styles => {:small => "150x150#", :medium => "320x280#", :large => "640x640>"}
  
  #validates_attachment_presence :photo
  validates_attachment_size :photo, :less_than => 2.megabytes  

  def photo_small_url
    photo.url(:small)
  end

  def photo_medium_url
    photo.url(:medium)
  end

  def photo_large_url
    photo.url(:large)
  end

  #def serializable_hash(options = {})
  #  super(options.merge({
  #    :methods => [:photo_small_url, :photo_medium_url, :photo_large_url]
  #  }))
  #end
end
