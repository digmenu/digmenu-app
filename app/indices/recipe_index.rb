# app/indices/article_index.rb
ThinkingSphinx::Index.define :recipe, :with => :active_record, :delta => true do
  indexes :name, :as => :recipe_name, sortable: true

  indexes restaurant.category, :as => :restaurant_category

  indexes category_tags(:name), :as => :category_tags
  indexes alert_tags(:name), :as => :alert_tags
  indexes taste_tags(:name), :as => :taste_tags

  has category_tags(:id), :as => :category_tag_ids, :facet => true
  has alert_tags(:id), :as => :alert_tag_ids, :facet => true
  has taste_tags(:id), :as => :taste_tag_ids, :facet => true

end