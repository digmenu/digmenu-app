module UsersHelper
  
  def current_user_following(followed)
    if current_user
      follow = current_user.following?(followed)
      return follow if follow
    end
    return ""
  end

end