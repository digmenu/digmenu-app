module RestaurantsHelper
  def like_count(recipes)
    numLikes = 0
    recipes.each do |recipe|
      numLikes = numLikes + recipe.likes.count
    end

    return numLikes
  end

  def starred_recipe_count(restaurant)
    totalRestaurantLikes = like_count(restaurant.recipes);
    starredCount = 0;

    restaurant.recipes.each do |recipe|
      if is_starred_with?(recipe, totalRestaurantLikes)
        starredCount = starredCount + 1;
      end
    end
    
    return starredCount;
  end

  def is_liked_by_current_user?(restaurant)
    return current_user.voted_up_for?(restaurant) if current_user
    return false
  end
end
