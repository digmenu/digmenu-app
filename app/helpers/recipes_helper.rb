module RecipesHelper

  def is_new?(recipe)
    recipe.created_at > 21.days.ago.utc
  end

  def is_starred?(recipe)
    numLikes = recipe.votes.size

    totalRestaurantLikes = like_count(recipe.restaurant.recipes)

    return (numLikes.to_f / totalRestaurantLikes) > 0.10
  end

  def is_starred_with?(recipe, totalRestaurantLikes)
    numLikes = recipe.votes.size

    return (numLikes.to_f / totalRestaurantLikes) > 0.10
  end  

  def is_liked_by_current_user?(recipe)
    return current_user.voted_up_for?(recipe) if current_user
    return false
  end
end
