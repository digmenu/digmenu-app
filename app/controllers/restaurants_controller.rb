class RestaurantsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show, :show_recipes]
  before_filter :check_for_mobile

  #respond_to :json

  # GET /restaurants
  # GET /restaurants.json
  def index
    @restaurants = Restaurant.page(params[:page]).per(10)

    respond_to do |format|
      format.html # index.html.erb
      format.json #{ render json: @restaurants }
    end
  end

  # GET /restaurants/1
  # GET /restaurants/1.json
  def show
    @restaurant = Restaurant.find(params[:id])
    @json = @restaurant.to_gmaps4rails
    respond_to do |format|
      format.html # show.html.erb
      format.json #{ render json: @restaurant }
    end
  end

  # GET /restaurants/new
  # GET /restaurants/new.json
  def new
    @restaurant = Restaurant.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @restaurant }
    end
  end

  # GET /restaurants/1/edit
  def edit
    @restaurant = Restaurant.find(params[:id])
  end

  # POST /restaurants
  # POST /restaurants.json
  def create
    @restaurant = Restaurant.new(params[:restaurant])

    respond_to do |format|
      if @restaurant.save
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully created.' }
        format.json { render json: @restaurant, status: :created, location: @restaurant }
      else
        format.html { render action: "new" }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /restaurants/1
  # PUT /restaurants/1.json
  def update
    @restaurant = Restaurant.find(params[:id])

    respond_to do |format|
      if @restaurant.update_attributes(params[:restaurant])
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /restaurants/1
  # DELETE /restaurants/1.json
  def destroy
    @restaurant = Restaurant.find(params[:id])
    @restaurant.destroy

    respond_to do |format|
      format.html { redirect_to restaurants_url }
      format.json { head :no_content }
    end
  end
  def vote_up
    @restaurant = Restaurant.find(params[:id])
    current_user.likes @restaurant
    respond_to do |format|
      format.html {redirect_to @restaurant, notice: "#{@restaurant.name} liked" }
      format.js
    end
  end

  # POST /restaurants/1/vote_up
  # POST /restaurants/1/vote_up.json
  def vote_up
    @restaurant = Restaurant.find(params[:id])
    if current_user.voted_up_for? @restaurant
      respond_to do |format|
        format.html {redirect_to @user}
        format.json {render status: :not_modified, json: {success: false}}
        format.js
      end
    else
      current_user.likes @restaurant
      respond_to do |format|
        format.html {redirect_to @user}
        format.json {render status: :ok, json: {success: true}}
        format.js
      end
    end
  end

  def show_users_upvoted
    @restaurant = Restaurant.find(params[:id])
    @votes = @restaurant.up_votes.page(params[:page]).per(params[:perPage])
    @voters = @votes.voters
    respond_to do |format|
      format.json { render 'show_users_upvoted' }
    end    
  end

  # GET /restaurants/1/recipes
  # GET /restaurants/1/recipes.json
  def show_recipes
    @restaurant = Restaurant.find(params[:id])
    @recipes = @restaurant.recipes.page(params[:page]).per(params[:perPage])
    respond_to do |format|
      format.json { render 'show_recipes' }
    end
  end

end
