class RecipesController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]
  # Render mobile or desktop depending on User-Agent for these actions.
  before_filter :check_for_mobile

  # GET /recipes
  # GET /recipes.json
  def index
    @recipes = Recipe.search(params[:q]).page(params[:page]).per(params[:perPage])

    respond_to do |format|
      format.html # index.html.erb
      format.json #{ render json: @recipes}
    end
  end

  # GET /recipes/1
  # GET /recipes/1.json
  def show
    @recipe = Recipe.find(params[:id])
    @restaurant = @recipe.restaurant

    respond_to do |format|
      format.html # show.html.erb
      format.json #{ render json: @recipe}
    end
  end

  # GET /recipes/new
  # GET /recipes/new.json
  def new
    @restaurant = Restaurant.find(params[:restaurant_id])
    @recipe = Recipe.new
    3.times {@recipe.recipe_images.build} # added this

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @recipe }
    end
  end

  # GET /recipes/1/edit
  def edit
    @recipe = Recipe.find(params[:id])
    #3.times {@recipe.recipe_images.build} # added this
  end

  # POST /recipes
  # POST /recipes.json
  def create
    @restaurant = Restaurant.find(params[:recipe][:restaurant_id])
    @recipe = @restaurant.recipes.create(params[:recipe])
    #@recipe = Recipe.new(params[:recipe])

    respond_to do |format|
      if @recipe.save
        format.html { redirect_to @recipe, notice: 'Recipe was successfully created.' }
        format.json { render json: @recipe, status: :created, location: @recipe }
      else
        format.html { render action: "new" }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /recipes/1
  # PUT /recipes/1.json
  def update
    @recipe = Recipe.find(params[:id])

    respond_to do |format|
      if @recipe.update_attributes(params[:recipe])
        format.html { redirect_to @recipe, notice: 'Recipe was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recipes/1
  # DELETE /recipes/1.json
  def destroy
    @recipe = Recipe.find(params[:id])
    @recipe.destroy

    respond_to do |format|
      format.html { redirect_to recipes_url }
      format.json { head :no_content }
    end
  end

  # POST
  # POST
  def vote_up
    @recipe = Recipe.find(params[:id])
    if current_user.voted_up_for? @recipe
      respond_to do |format|
        format.html {redirect_to @user}
        format.json {render status: :not_modified, json: {success: false}}
        format.js
      end
    else
      current_user.likes @recipe
      respond_to do |format|
        format.html {redirect_to @user}
        format.json {render status: :ok, json: {success: true}}
        format.js
      end
    end
  end

  def show_users_upvoted
    @recipe = Recipe.find(params[:id])
    @votes = @recipe.up_votes.page(params[:page]).per(params[:perPage])
    @voters = @votes.voters
    respond_to do |format|
      format.json { render 'show_users_upvoted' }
    end    
  end

end
