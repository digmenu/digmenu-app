class HomeController < ApplicationController
  before_filter :check_for_mobile
  
  def index
    @users = User.all
    @restaurants = Restaurant.all

    if mobile_device?
      puts ">> mobile device"
    else
      @recipes = Recipe.all
      puts ">> desktop device"
    end

    respond_to do |format|
      format.html #index.html.erb
      format.json {render json: @users}
    end
  end
end
