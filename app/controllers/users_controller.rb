class UsersController < ApplicationController
  before_filter :authenticate_user!
  before_filter :check_for_mobile

  def index
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @users = User.page(params[:page]).per(params[:perPage])
  end

  def show
    @user = User.find(params[:id])
    @recipes = @user.find_up_voted_items
    respond_to do |format|
      format.html # show.html.erb
      format.json #{ render json: @user}
    end    
  end
  
  def update
    authorize! :update, @user, :message => 'Not authorized as an administrator.'
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user], :as => :admin)
      redirect_to users_path, :notice => "User updated."
    else
      redirect_to users_path, :alert => "Unable to update user."
    end
  end
    
  def destroy
    authorize! :destroy, @user, :message => 'Not authorized as an administrator.'
    user = User.find(params[:id])
    unless user == current_user
      user.destroy
      redirect_to users_path, :notice => "User deleted."
    else
      redirect_to users_path, :notice => "Can't delete yourself."
    end
  end

  def following
    @title = "Following"
    @user = User.find(params[:id])
    @users = @user.following.page(params[:page]).per(params[:perPage])
    render 'show_follow'
  end
  
  def followers
    @title = "Followers"
    @user = User.find(params[:id])
    @users = @user.followers.page(params[:page]).per(params[:perPage])
    render 'show_follow'
  end

  def liked_recipes
    @user = User.find(params[:id])
    @votes = @user.get_up_voted(Recipe).page(params[:page]).per(params[:perPage])

    respond_to do |format|
      format.json { render 'show_upvote' }
    end
  end

  def liked_restaurants
    @user = User.find(params[:id])
    @votes = @user.get_up_voted(Restaurant).page(params[:page]).per(params[:perPage])
    
    respond_to do |format|
      format.json { render 'show_upvote' }
    end
  end

end