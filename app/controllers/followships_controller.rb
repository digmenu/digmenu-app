class FollowshipsController < ApplicationController
  before_filter :authenticate_user!  
  before_filter :check_for_mobile
  
  def create
    @user = User.find(params[:followship][:followed_id])
    @followship = current_user.follow!(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.json { render status: :ok, json: @followship}
      format.js
    end
  end
  
  def destroy
    @user = Followship.find(params[:id]).followed
    current_user.unfollow!(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.json {render status: :ok, json: {success: true}}      
      format.js
    end
  end
end
