module Api
  class SessionsController < Devise::SessionsController

    def create
      resource = warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#failure")
      sign_in(resource_name, resource)
      resource.ensure_authentication_token!
      return render :status => 200,
               :json => { :success => true,
                          :info => "Logged in",
                          :authentication_token => resource.authentication_token, 
                          :user_id => resource.id }
    end

    def destroy
      resource = warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#failure")
      sign_out(resource_name)
      resource.reset_authentication_token!
      return render :status => 200,
               :json => { :success => true,
                          :info => "Logged out",
                          :data => {} }
    end

    def failure
      render :status => 401,
             :json => { :success => false,
                        :info => "Login Failed",
                        :data => {} }
    end
  end
end