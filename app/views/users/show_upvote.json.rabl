object false

node (:user_id) { |m| @user.id }

node (:total) { |m| @votes.total_count }
node (:total_pages) { |m| @votes.num_pages }
node (:page_num) { |m| @votes.current_page }

child @votes => :liked do
  attributes :id, :avatar_url
end
