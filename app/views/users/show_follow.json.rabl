object false

node (:total) { |m| @users.total_count }
node (:total_pages) { |m| @users.num_pages }
node (:page_num) { |m| @users.current_page }

child @users => :follows do
  attributes :id, :avatar_url
end