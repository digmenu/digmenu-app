object @user
attributes :id, :name, :avatar_url, :avatar_medium_url

node (:followship) do |user|
  current_user_following(user)
end

node (:follower_count) do |user|
  user.followers.count
end

node (:following_count) do |user|
  user.following.count
end

node (:liked_recipe_count) do |user|
  user.get_up_voted(Recipe).count
end