object false

node (:restaurant_id) { |m| @restaurant.id }

node (:total) { |m| @recipes.total_count }
node (:total_pages) { |m| @recipes.num_pages }
node (:page_num) { |m| @recipes.current_page }

child @recipes => :recipes do
  attributes :id, :name, :avatar_url
  
  node :like_count do |recipe|
    recipe.votes.size
  end

  child :recipe_images do
    attributes :photo_small_url, :photo_medium_url, :photo_large_url
  end
end