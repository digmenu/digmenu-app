attributes :id, :name, :category, :location, 
  :avatar_url, :avatar_medium_url, 
  :latitude, :longitude


node :recipe_count do |restaurant|
  restaurant.recipes.count
end

node :starred_recipe_count do |restaurant|
  starred_recipe_count restaurant
end

node :like_count do |restaurant|
  restaurant.likes.count
end

node :is_liked_by_me do |restaurant|
  is_liked_by_current_user?(restaurant)
end