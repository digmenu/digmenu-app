object @restaurants

node (:total) { |m| @restaurants.total_count }
node (:total_pages) { |m| @restaurants.num_pages }
node (:page_num) { |m| @restaurants.current_page }

attributes :category, :name, :location

child :recipes do
  attributes :name
  child :recipe_images do
    attributes :photo_small_url, :photo_medium_url, :photo_large_url
  end
end