object @restaurant

node (:restaurant_id) { |m| @restaurant.id }

node (:total) { |m| @recipes.total_count }
node (:total_pages) { |m| @recipes.num_pages }
node (:page_num) { |m| @recipes.current_page }

extends "restaurants/api/item"