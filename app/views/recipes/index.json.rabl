object false

node (:total) { |m| @recipes.total_count }
node (:total_pages) { |m| @recipes.num_pages }
node (:page_num) { |m| @recipes.current_page }

child @recipes do
  extends "recipes/api/item"
end