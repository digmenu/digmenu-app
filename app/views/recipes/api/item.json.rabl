attributes :id, :name

node :like_count do |recipe|
  recipe.votes.size
end

node :is_new do |recipe|
  is_new?(recipe)
end

node :is_starred do |recipe|
  is_starred?(recipe)
end

child :restaurant do
  attributes :id, :name
end

node :is_liked_by_me do |recipe|
  is_liked_by_current_user?(recipe)
end

child :recipe_images do
  attributes :photo_small_url, :photo_medium_url, :photo_large_url
end