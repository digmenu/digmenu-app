object false

node (:recipe_id) { |m| @recipe.id }

node (:total) { |m| @votes.total_count }
node (:total_pages) { |m| @votes.num_pages }
node (:page_num) { |m| @votes.current_page }

child @voters => :voters do
  attributes :id, :name, :avatar_url
end