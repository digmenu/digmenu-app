source 'https://rubygems.org'

gem 'rails', '3.2.12'
gem 'mysql2', '0.3.12b6'
gem 'pg', '~>0.14.1'
gem "thin", ">= 1.5.0"

group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'
  gem 'uglifier', '>= 1.0.3'
  gem 'handlebars_assets',  '>= 0.4.4'
  gem 'debugger', '>= 1.3.1'
  gem 'eco', '~> 1.0.0'
end

group :development do
  gem "better_errors", ">= 0.3.2"
  gem "binding_of_caller", ">= 0.6.8"
  gem "rails-erd", '~> 1.1.0'
  gem "haml-rails", ">= 0.4"
  gem "hpricot", ">= 0.8.6"
  gem "ruby_parser", ">= 3.1.1"
  gem "quiet_assets", ">= 1.0.1"
  gem "rspec-rails", ">= 2.12.2"
  gem "factory_girl_rails", ">= 4.1.0"
end

group :test do
  gem "launchy", ">= 2.1.2"
  gem "capybara", ">= 2.0.2"
  gem "cucumber-rails", ">= 1.3.0", :require => false  
  gem "database_cleaner", ">= 0.9.1"
  gem "email_spec", ">= 1.4.0"
  gem "mocha"
  gem "rspec-rails", ">= 2.12.2"
  gem "factory_girl_rails", ">= 4.1.0"  
end

# misc
gem "ancestry", "~> 1.3.0"

# Front-end UI 
gem 'jquery-rails', '~> 2.2.1'
gem "bootstrap-sass", ">= 2.2.2.0"
gem "therubyracer", "~> 0.11.3"
gem 'web-app-theme', :git =>'git://github.com/pilu/web-app-theme.git'

# Authentication/Authorization
gem "devise", ">= 2.2.3"
gem 'omniauth', ">= 1.1.1"
gem 'omniauth-facebook', ">= 1.4.1"
gem 'omniauth-twitter', ">= 0.0.14"
gem "cancan", ">= 1.6.8"
gem "rolify", ">= 3.2.0"

gem "simple_form", ">= 2.0.4"
gem "figaro", ">= 0.5.3"
gem "cocaine", ">= 0.3.2"
gem "mini_magick"
gem "paperclip", ">= 3.4.0"
gem 'acts_as_votable', '~> 0.5.0'
gem 'acts-as-taggable-on', '~> 2.3.3'

# Pagonator
gem 'kaminari', '~> 0.14.1'

# backend-frontend interface
gem 'rabl', '~> 0.7.9'
gem 'oj', '~> 2.0.3'
gem "haml", "~> 4.0.0"
gem "turbolinks", "~> 1.0.0"

# Admin
gem "nifty-generators", "~> 0.4.6"
gem 'rails_admin', :git => 'git://github.com/sferik/rails_admin.git'

# Search
gem 'riddle', '~>1.5.5'
gem 'thinking-sphinx', '~>3.0.1', :require => 'thinking_sphinx'

# Geolocation
gem "gmaps4rails", "~> 1.5.6"
gem 'geocoder', '~> 1.1.6'
gem 'google-analytics-rails', '>= 0.0.4'
gem 'garb', :git => 'git://github.com/Sija/garb.git'
gem 'google_visualr', '>= 2.1.7'
