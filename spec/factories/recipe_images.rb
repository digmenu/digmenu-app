# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :recipe_image do
    caption "MyString"
    recipe_id 1
  end
end
