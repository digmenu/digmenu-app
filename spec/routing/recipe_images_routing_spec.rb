require "spec_helper"

describe RecipeImagesController do
  describe "routing" do

    it "routes to #index" do
      get("/recipe_images").should route_to("recipe_images#index")
    end

    it "routes to #new" do
      get("/recipe_images/new").should route_to("recipe_images#new")
    end

    it "routes to #show" do
      get("/recipe_images/1").should route_to("recipe_images#show", :id => "1")
    end

    it "routes to #edit" do
      get("/recipe_images/1/edit").should route_to("recipe_images#edit", :id => "1")
    end

    it "routes to #create" do
      post("/recipe_images").should route_to("recipe_images#create")
    end

    it "routes to #update" do
      put("/recipe_images/1").should route_to("recipe_images#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/recipe_images/1").should route_to("recipe_images#destroy", :id => "1")
    end

  end
end
