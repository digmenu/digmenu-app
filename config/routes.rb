DigMenu::Application.routes.draw do

  devise_for :users

  namespace :api do
    devise_scope :user do
      post 'users/sign_in' => 'sessions#create'
      delete 'users/sign_out' => 'sessions#destroy'
    end

    resources :recipes do
      member do
        post '/vote_up' => 'recipes#vote_up'
      end
    end
  end

  resources :users do
    member do
      get :following, :followers, :liked_recipes, :liked_restaurants
    end
  end

  resources :followships,  :only => [:create, :destroy]

  get "admin/show"

  resources :restaurants do
    member do
      get '/recipes' => 'restaurants#show_recipes'
      get '/users_upvoted' => 'restaurants#show_users_upvoted'
      post '/vote_up' => 'restaurants#vote_up'
    end
  end
  
  resources :recipes do
    member do
      post '/like' => 'recipes#vote_up'
      get '/users_upvoted' => 'recipes#show_users_upvoted'      
      post '/vote_up' => 'recipes#vote_up'
    end
  end

  authenticated :user do
    root :to => 'home#index'
  end
  root :to => "home#index"

end