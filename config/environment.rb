# Load the rails application
require File.expand_path('../application', __FILE__)

DigMenu::Application.configure do  
  config.assets.paths << "#{ Rails.root }/app/assets/javascript/mobile/templates"

  config.gem 'thinking-sphinx', :version => '3.0.1', :lib => 'thinking_sphinx'
end

# Initialize the rails application
DigMenu::Application.initialize!
