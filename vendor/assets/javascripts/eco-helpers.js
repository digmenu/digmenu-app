// Must include eco-helpers.js before eco files
 
(function(global) {
  var ecoHelpers = {
    sanitize: function(value) {
      if (value && value.ecoSafe) {
        return value;
      } else if (typeof value !== 'undefined' && value != null) {
        return ecoHelpers.escape(value);
      } else {
        return '';
      }
    },
 
    safe: function(value) {
      if (value && value.ecoSafe) {
        return value;
      } else {
        if (!(typeof value !== 'undefined' && value != null)) value = '';
        var result = new String(value);
        result.ecoSafe = true;
        return result;
      }
    },
 
    escape: function(value) {
      return ('' + value)
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;');
    },
 
    captureFor: function(obj, out) {
      return (function(callback) {
        var length = out.length;
        callback.call(obj);
        return ecoHelpers.safe(out.splice(length, out.length - length).join(''));
      });
    }
  };
 
  global.ecoHelpers = ecoHelpers;
})(window);