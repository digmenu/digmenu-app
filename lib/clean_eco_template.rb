require 'sprockets/eco_template'
 
class CleanEcoTemplate < Sprockets::EcoTemplate
  FROM = "  (function() {"
  TO = "}).call(__obj);"
 
  def evaluate(scope, locals, &block)
    content = Eco.compile(data)
    from = content.index(FROM)
    to = content.rindex(TO)
    content = content[from...to] + TO
    <<-JS
function(__obj) {
  if (!__obj) __obj = {};
  var __helpers = window.ecoHelpers;
  var __out = [];
  var __sanitize = __helpers.sanitize;
  var __capture = __helpers.captureFor(__obj, __out);
  var __rememberSafe = __obj.safe;
  var __rememberEscape = __obj.escape;
  __obj.safe = __helpers.safe;
  __obj.escape = __helpers.escape;
#{content}
  __obj.safe = __rememberSafe;
  __obj.escape = __rememberEscape;
  return __out.join('');
};
      JS
  end
end