class AddDeltaToRecipes < ActiveRecord::Migration
  def change
    add_column :recipes, :delta, :boolean, :default => true, :null => false
  end
end
